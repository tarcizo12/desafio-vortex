
val acceptedInputs = listOf('I', 'V', 'X', 'L', 'C', 'D', 'M') //Lista de algarismos romanos permitidos

fun convert(digit: String): Int {
    val listOfDigit = digit.toCharArray()
    val listOfDigitConverted = arrayListOf<Int>()
    var valueOfSum = 0
    var i = 0

    //Pega a lista de algarismos romanos e separa em numeros decimais
    for (element in listOfDigit) {

        if (!acceptedInputs.contains(element)) {
            println(" Simbolo invalido detectado ($element), tente novamente \n")
            break
        }
        listOfDigitConverted.add(convertRomanNumeral(element))

    }

    //Percorre a lista convertida e muda o sinal caso o numero atual seja menor que o seguite (Regra de subtração)
    while (i < listOfDigitConverted.size - 1) {
        val current = listOfDigitConverted[i]
        val next = listOfDigitConverted[i + 1]

        if (current < next) {
            listOfDigitConverted[i] = current * -1
        }

        i++
    }

    //Realiza a soma de toda a lista convertida com os seus devidos sinais de operação
    for (element in listOfDigitConverted) {
        valueOfSum += element
    }

    return valueOfSum
}

fun convertRomanNumeral(digit: Char): Int {
    var number = 0

    when (digit) {
        'I' -> number = 1
        'V' -> number = 5
        'X' -> number = 10
        'L' -> number = 50
        'C' -> number = 100
        'D' -> number = 500
        'M' -> number = 1000
    }

    return number
}

private fun prompt(message: String): String {
    print(message)
    return readLine() ?: ""
}

fun main() {

    while (true) {
        val numberRoman = prompt("\n Digite o numero romano para a conversao ")
        val numberConverted = convert(numberRoman)

        if ( numberConverted > 0) {
            println(" O numero convertido fica: $numberConverted \n")
        }

        val confirm = prompt(" Deseja realizar outra operacao? \n y|n ")

        if (confirm == "y"|| confirm == "Y") {
            continue
        } else if (confirm == "n"|| confirm == "N") {
            println("\n Ok, ate logo!")
            break
        } else {
            println("\n Comando invalido tente, novamente")
            continue
        }

    }

}

