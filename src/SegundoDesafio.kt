
private fun createMatriz(numberOfStars: Int): Array<IntArray> {
    val matriz = Array(numberOfStars) { IntArray(numberOfStars) }

    //Criando matriz quadrada com a mesma quantidade de linhas e colunas baseado na quantidade de estrelas

    for (i in 0 until numberOfStars) {
        for (j in 0 until numberOfStars) {
            if (i == j) {
                matriz[i][j] = 0
            } else {
                matriz[i][j] = (0..1).random() //Gera randomicamente 0 ou 1
            }
        }
    }

    return matriz
}

private fun printMatriz(arrayMatriz: Array<IntArray>) {
    val rows = arrayMatriz.size
    val columns = arrayMatriz[0].size

    for (i in 0 until rows) {
        for (j in 0 until columns) {
            print("${arrayMatriz[i][j]} ")
        }
        println()
    }
}

private fun verificaLigacao(arrayMatriz: Array<IntArray>, firstStar: Int, secondStar: Int): String {
    val boolean = arrayMatriz[firstStar][secondStar] == 1 //Variavel para determinar interseção

    return if (boolean) {
        ",ha ligacao"
    } else {
        ",nao ha ligacao"
    }

}

private fun promptInt(message: String): Int {
    print(message)
    return readLine()?.toInt() ?: 0
}

fun main() {

    while (true) {
        //Input da quantidade de estrelas
        val numberOfStars = promptInt(" Digite a quantidade de estrelas desejada ")

        if (numberOfStars !in 4..8) {
            println("\n Valor invalido! Digite um entre 4 a 8")
            continue
        } else {
            val matriz = createMatriz(numberOfStars)

            println("\n As estrelas sao de 0 a ${numberOfStars - 1}")

            //Input das estrelas
            val firstStar = promptInt(" Digite a primeira estrela a pesquisar ")
            val secondStar = promptInt(" Digite a segunda estrela a pesquisar ")

            println()
            printMatriz(matriz)
            println( verificaLigacao(matriz, firstStar, secondStar) )
            break
        }
        
    }

}