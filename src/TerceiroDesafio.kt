//Variáveis constantes em escopo globall
val suit = arrayOf("C", "E", "O", "P")
val cheap = Cheap()// Baralho completo
val tableDeck = cheap.randomizeCards(5)//Baralho da mesa
val deckOfRobotOne = cheap.randomizeCards(2)//Baralho do robo 1
val deckOfRoboTwo = cheap.randomizeCards(2)//Baralho do robo 2
val movesRobotOne = Moves(deckOfRobotOne, tableDeck)
val movesRobotTwo = Moves(deckOfRoboTwo, tableDeck)
var bestMoveOfRobotOne = movesRobotOne.bestMove()
var bestMoveOfRobotTwo = movesRobotTwo.bestMove()
val moveOfRobotOne = identifyMove(bestMoveOfRobotOne)
val moveOfRobotTwo = identifyMove(bestMoveOfRobotTwo)

class Card(var numberOfCard: Int, var suit: String)

class Cheap {
    private val cheapOfCards = mutableListOf<Card>()
    private val sizeOfCheap = this.cheapOfCards.size

    init {
        createCheap()
        shuffleCheap()
    }

    private fun createCheap(): MutableList<Card> {

        //Gera as 52 cartas de acordo
        for (i in 1..13) {
            val hearts = Card(i, suit[0])
            val spades = Card(i, suit[1])
            val diamonds = Card(i, suit[2])
            val clubs = Card(i, suit[3])

            this.cheapOfCards.addAll(listOf(hearts, spades, diamonds, clubs))
        }

        return this.cheapOfCards
    }

    private fun shuffleCheap() {

        //Embaralha as cartas para começar o jogo
        for (i in 0..500) {
            val random1 = (0..25).random()
            val random2 = (26..51).random()

            val aux1 = this.cheapOfCards[random1]
            this.cheapOfCards[random1] = this.cheapOfCards[random2]
            this.cheapOfCards[random2] = aux1

        }

    }

    fun randomizeCards(numberOfCard: Int): MutableList<Card> {
        val listOfRandomCards = mutableListOf<Card>()

        for (i in 0 until numberOfCard) {
            val randomizeNumber = (0..sizeOfCheap).random()
            val randomCard = this.cheapOfCards.removeAt(randomizeNumber)
            listOfRandomCards.add(randomCard)
        }

        return listOfRandomCards
    }

    fun viewCheap(cheap: MutableList<Card>) {

        //Método de sobrecarga
        for (elements in cheap) {
            if (elements.numberOfCard in 2..10) {
                print("${elements.numberOfCard}${elements.suit} ")
            } else {
                print("${convertView(elements)}${elements.suit} ")
            }
        }
        println()
    }

    private fun convertView(element: Card): String {
        var value = ""

        //Converte o valor das cartas ao exibir para as letras com o peso equivalente
        when (element.numberOfCard) {
            1 -> value = "A"
            11 -> value = "J"
            12 -> value = "Q"
            13 -> value = "K"
        }

        return value
    }

}

class Moves(robotDeck: MutableList<Card>, tableDeck: MutableList<Card>) {
    var deckToCheck = mutableListOf<Card>()
    private val deckRobot = robotDeck
    private val deckTable = tableDeck
    private var sizeDeck = this.deckToCheck.size
    val numbersOfDeck = mutableListOf<Int>()


    fun bestMove(): Int {

        val robot = this.deckRobot
        val table = this.deckTable
        var bestSequence = mutableListOf<Card>()
        var bestMovement = 0
        var x = 0
        var y = x + 1
        var z = y + 1
        var k: Int

        //Implementação da combinação de 5,3
        while (x < table.size) {
            while (y < table.size) {
                while (z < table.size) {
                    if (x != y && x != z && y != z) {
                        this.deckToCheck = mutableListOf(table[x], table[y], table[z], robot[0], robot[1])
                        this.sizeDeck = deckToCheck.size
                        this.numbersOfDeck.clear()

                        for (elements in deckToCheck) {
                            this.numbersOfDeck.add(elements.numberOfCard)
                        }

                        organizeDeck()

                        if (identifiesMovement() > bestMovement) {
                            bestMovement = identifiesMovement()
                            bestSequence = this.deckToCheck
                        }

                    }
                    z++
                }
                z = y + 1
                y++
            }
            y = x + 1
            x++
        }

        //Reseta variaveis para fazer uma nova verificação de combinações
        x = 0
        y = x + 1
        z = y + 1
        k = z + 1

        //Combinação de 5,4 com 2,1
        while (x < table.size) {
            while (y < table.size) {
                while (z < table.size) {
                    while (k < table.size) {
                        if (x != y && x != k && x != z && y != z && y != k && k != z) {
                            this.deckToCheck = mutableListOf(table[x], table[y], table[z], table[k], robot[0])
                            this.sizeDeck = deckToCheck.size
                            this.numbersOfDeck.clear()

                            for (elements in deckToCheck) {
                                this.numbersOfDeck.add(elements.numberOfCard)
                            }

                            organizeDeck()

                            if (identifiesMovement() >= bestMovement) {
                                bestMovement = identifiesMovement()
                                bestSequence = this.deckToCheck
                            }

                            //Muda a ultima carta para realizar a permutação das 2 cartas do robo com as 4 da mesa
                            this.deckToCheck[4] = robot[1]
                            this.numbersOfDeck.clear()

                            for (elements in deckToCheck) {
                                this.numbersOfDeck.add(elements.numberOfCard)
                            }

                            organizeDeck()
                            if (identifiesMovement() >= bestMovement) {
                                bestMovement = identifiesMovement()
                                bestSequence = this.deckToCheck
                            }

                        }
                        k++
                    }
                    k = z + 1
                    z++
                }
                z = y + 1
                y++
            }
            y = x + 1
            x++
        }

        this.deckToCheck = bestSequence
        return bestMovement

    }

    private fun identifiesMovement(): Int {
        //Verifica qual jogada o deck pode fazer e atribui a um valor adequeado
        val moveIs: Int = when {
            //Mantem hierarquia dos movimentos
            isRoyalFlush() -> 10
            isStraightFlush() -> 9
            isFour() -> 8
            isFullHouse() -> 7
            isFlush() -> 6
            isStraight() -> 5
            isTrio() -> 4
            isTwoPairs() -> 3 //Ver
            isPair() -> 2
            else -> 0
        }

        return moveIs
    }

    private fun isRoyalFlush(): Boolean {
        //Verifica se o deck possui a sequencia correta
        val sequenceOfRoaylFlush = listOf(1, 10, 11, 12, 13)
        val sameSuits = isSameSuits()

        return this.numbersOfDeck.containsAll(sequenceOfRoaylFlush) && sameSuits

    }

    private fun isStraightFlush(): Boolean = isSequence() && isSameSuits() && !this.numbersOfDeck.contains(1)

    private fun isFour(): Boolean {
        var flag = false

        for (elements in this.numbersOfDeck) {
            val repeatedTimes = repeatedNumbers(elements)
            if (repeatedTimes == 4) {
                flag = true
            }
        }

        return flag
    }

    private fun isFullHouse(): Boolean {
        var threeSameCards = false
        var twoSameCards = false

        for (elements in numbersOfDeck) {
            if (repeatedNumbers(elements) == 3) {
                threeSameCards = true
            } else if (repeatedNumbers(elements) == 2) {
                twoSameCards = true
            }
        }


        return (threeSameCards && twoSameCards)
    }

    private fun isFlush(): Boolean = isSameSuits() && !isSequence()

    private fun isStraight(): Boolean = !isSameSuits() && isSequence()

    private fun isTrio(): Boolean {
        var threeSameCards = false
        var twoDifferentCards = false

        for (elements in this.numbersOfDeck) {
            if (repeatedNumbers(elements) == 3) {
                threeSameCards = true
            } else if (repeatedNumbers(elements) <= 1) {
                twoDifferentCards = true
            }
        }

        return threeSameCards && twoDifferentCards
    }

    private fun isTwoPairs(): Boolean {
        val listOfPairs = mutableListOf<Int>()

        for (elements in this.numbersOfDeck) {
            if (isPair() && !listOfPairs.contains(elements)) {
                listOfPairs.add(elements)
            }
        }

        return listOfPairs.size == 2
    }

    private fun isPair(): Boolean {
        var twoSameCards = 0
        var threeDifferentCards = 0

        for (elements in this.numbersOfDeck) {
            if (repeatedNumbers(elements) == 2) {
                twoSameCards++
            }
            if (repeatedNumbers(elements) <= 1) {
                threeDifferentCards++
            }
        }

        return threeDifferentCards == 3 && twoSameCards == 2
    }

    private fun organizeDeck() {
        //Organiza o deck combinado em ordem crescente
        this.deckToCheck.sortWith { x, y ->
            x.numberOfCard - y.numberOfCard
        }
    }

    private fun isSameSuits(): Boolean {
        return this.deckToCheck[0].suit == this.deckToCheck[1].suit && this.deckToCheck[1].suit ==
                this.deckToCheck[2].suit && this.deckToCheck[2].suit == this.deckToCheck[3].suit &&
                this.deckToCheck[3].suit == this.deckToCheck[4].suit
    }

    private fun isSequence(): Boolean {
        var sequence = 1
        var boolean = false
        var i = 0

        while (i < this.sizeDeck - 1) {
            val currentElement = this.deckToCheck[i]
            val nextElement = this.deckToCheck[i + 1]
            val difference = nextElement.numberOfCard - currentElement.numberOfCard

            if (difference == 1) {
                sequence++
            }

            if (sequence == 5) {
                boolean = true
            }
            i++
        }

        return boolean
    }

    private fun repeatedNumbers(number: Int): Int {
        var repeatedTimes = 0

        for (i in 0 until this.numbersOfDeck.size){
            if(this.numbersOfDeck[i]==number){
                repeatedTimes++
            }
        }

        return repeatedTimes
    }

}

fun identifyMove(number: Int): String {
    var movement = ""
    when (number) {

        10 -> movement = "Royal Flush"
        9 -> movement = "Straight Flush"
        8 -> movement = "Quadra"
        7 -> movement = "Full House"
        6 -> movement = "Flush"
        5 -> movement = "Straight"
        4 -> movement = "Trio"
        3 -> movement = "Dois pares" //Ver
        2 -> movement = "Par"
        0 -> movement = "Carta alta"
    }

    return movement

}

fun main() {

    //Apresentação dos casoss de vitoria
    if (bestMoveOfRobotOne > bestMoveOfRobotTwo) {
        print("Robo 1: $moveOfRobotOne || ")
        cheap.viewCheap(movesRobotOne.deckToCheck)
        print("Robo 2: $moveOfRobotTwo || ")
        cheap.viewCheap(movesRobotTwo.deckToCheck)
        print("Robo 1 venceu!")
    } else if (bestMoveOfRobotTwo > bestMoveOfRobotOne) {
        print("Robo 1: $moveOfRobotOne || ")
        cheap.viewCheap(movesRobotOne.deckToCheck)
        print("Robo 2: $moveOfRobotTwo || ")
        cheap.viewCheap(movesRobotTwo.deckToCheck)
        print("Robo 2 venceu! ")
    } else if (bestMoveOfRobotTwo == bestMoveOfRobotOne && bestMoveOfRobotOne !=0) {
        print("Robo 1: $moveOfRobotOne || ")
        cheap.viewCheap(movesRobotOne.deckToCheck)
        print("Robo 2: $moveOfRobotTwo || ")
        cheap.viewCheap(movesRobotTwo.deckToCheck)
        print("Tivemos um empate !!")
    } else if (bestMoveOfRobotOne == 0 && bestMoveOfRobotTwo == 0) {
        //Verifica caso de empate

        val numbersOfRobotOne = movesRobotOne.numbersOfDeck
        val numbersOfRobotTwo = movesRobotTwo.numbersOfDeck
        var highCard = 0

        numbersOfRobotOne.forEach { number ->
            if (number > highCard) {
                highCard = number
            }
        }

        numbersOfRobotTwo.forEach { number ->
            if (number > highCard) {
                highCard = number
            }
        }

        //Verifica seu o deck tem a maior carta ou o caso de ter o ''As''
        if (numbersOfRobotOne.contains(highCard) || numbersOfRobotOne.contains(1)) {
            print("Robo 1: $moveOfRobotOne || ")
            cheap.viewCheap(movesRobotOne.deckToCheck)
            print("Robo 2: $moveOfRobotTwo || ")
            cheap.viewCheap(movesRobotTwo.deckToCheck)
            print("Robo 1 venceu com a carta mais alta de numero: ${highCard}!")
        } else if (numbersOfRobotTwo.contains(highCard) || numbersOfRobotTwo.contains(1)) {
            print("Robo 1: $moveOfRobotOne || ")
            cheap.viewCheap(movesRobotOne.deckToCheck)
            print("Robo 2: $moveOfRobotTwo || ")
            cheap.viewCheap(movesRobotTwo.deckToCheck)
            print("Robo 2 venceu com a carta mais alta de numero: ${highCard}!")
        }

    }

}